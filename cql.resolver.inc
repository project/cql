<?php
/**
 * @file
 * @brief
 * The set of CQL IRT resolvers.
 *
 * Abstract class for class which resolve IRT search clauses
 * into a specific external language
 *
 * @package 	CQL
 * @subpackage	resolvers
 * @author
 */

abstract class irtResolver {
  protected $indexes = array();
  protected $relType = NULL;

  /**
   * Description needed.
   */
  abstract protected function resolveIndex(&$irtPack, $idx, $i, &$r, $t, &$joins);

  /**
   * Description needed.
   */
  abstract protected function resolveRelation(&$irtPack, $rel, $i, $r, $t);

  /**
   * Description needed.
   */
  final protected function stripquotes($x) {
    while (drupal_substr($x, 0, 1) == '"' && drupal_substr($x, -1) == '"') {
      $x = drupal_substr($x, 1, drupal_strlen($x)-2);
    }
    return $x;
  }
}
