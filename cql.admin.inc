<?php

/**
 * @file
 * @brief
 *   Form settings
 *
 * @package     cql
 * @author
 */

/**
 * Menu callback for the settings form.
 */
function cql_get_admin_form() {
  $form['cql'] = array(
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['cql']['cql_forms'] = array(
   '#type' => 'checkboxes',
   '#title' => t('Content Types'),
   '#description' => t('Select content types which will be used by CQL.'),
   '#options' => node_get_types('names'),
   '#default_value' => drupal_map_assoc(variable_get('cql_forms', array())),
  );

  return system_settings_form($form);
}

