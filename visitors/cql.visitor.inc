<?php

/**
 * @file
 * @brief
 * Basic abstract Visitor pattern for Parsed CQL objects
 *
 * @details
 *
 * @package CQL
 * @subpackage
 * @author
 */

class irtPackage {
  private $indexes = array();
  private $output = '' ;
  private $done = FALSE;

  /**
   * Description needed.
   */
  public function add_index($i) {
    if (!is_array($i)) {
      $a[$i] = NULL;
    }
    else {
      $a = $i;
    }
    foreach ($a as $idx => $v) {
      $this->indexes[$idx] = $idx;
    }
  }

  /**
   * Description needed.
   */
  public function add_output($op) {
    $this->output .= $op;
  }

  /**
   * Description needed.
   */
  public function set_done() {
    $this->done = TRUE;
  }

  /**
   * Description needed.
   */
  public function is_done() {
    return $this->done;
  }

  /**
   * Description needed.
   */
  public function get_indexes() {
    return $this->indexes;
  }

  /**
   * Description needed.
   */
  public function __toString() {
    return $this->output;
  }
}

abstract class cqlVisitor {
  static public $engine;

  protected $joins = array();

  protected $resolvers = array();

  /**
   * Description needed.
   */
  protected function __construct($type) {
    $this->resolvers = contextSet::fetch_resolvers($type);
    self::$engine = $type;
  }

  /**
   * Description needed.
   */
  public function visit(Visitation $visitee, $depth) {
    $method = 'visit_' . get_class($visitee);
    if (method_exists($this, $method)) {
      $this->$method($visitee, $depth);
    }
  }

  /**
   * Description needed.
   */
  abstract protected function visit_Triple(Triple $visitee, $depth);

  /**
   * Description needed.
   */
  abstract protected function visit_xBoolean(xBoolean $visitee, $depth);

  /**
   * Description needed.
   */
  abstract protected function visit_searchClause(searchClause $visitee, $depth);

  /**
   * Description needed.
   */
  abstract protected function visit_Index(Index $visitee, $depth);

  /**
   * Description needed.
   */
  abstract protected function visit_Relation(Relation $visitee, $depth);

  /**
   * Description needed.
   */
  abstract protected function visit_Term(Term $visitee, $depth);

  /**
   * Description needed.
   */
  abstract protected function visit_PrefixableObject(PrefixableObject $visitee, $depth);

  /**
   * Description needed.
   */
  abstract protected function visit_ModifiableObject(ModifiableObject $visitee, $depth);

  /**
   * Description needed.
   */
  abstract protected function visit_ModifierClause(ModifierClause $visitee, $depth);

  /**
   * Description needed.
   */
  protected function despatchIRT($i, $r, $t) {
    $irtPack = new irtPackage();
    list($cs, $idx) = explode('.', $i);
    if ($resolver = $this->resolvers[$cs]) {
      $resolver->resolveIndex($irtPack, $idx, $i, $r, $t, $this->joins);
    }
    if (!$irtPack->is_done()) {
      list($cs, $rel) = explode('.', $r);
      if ($resolver = $this->resolvers[$cs]) {
        $resolver->resolveRelation($irtPack, $rel, $i, $r, $t);
      }
    }

    if (!$irtPack->is_done()) {
      $attributes = array('!idx' => $i, '!rel' => $r, '!trm' => $t);
      throw new Exception(t('Unable to complete search clause using !idx, !rel, !trm', $attributes));
    }
    return (string) $irtPack;
  }
}
