<?php

/**
 * @file
 * @brief
 * Visit all the nodes in a parsed CQL structure and
 * generate the MySQL WHERE clause output for that structure
 *
 * @details
 *
 * @package CQL
 * @subpackage
 * @author
 */

define('CQL_TEMP_TABLE_NAME', 'viddies');

_cql_load_visitor_file();

class cqlVisitorGenMySQL extends cqlVisitor {
  private $sql = '';

  /**
   * Description needed.
   */
  public function __construct() {
    parent::__construct('MySQL');
  }

  /**
   * Description needed.
   */
  protected function visit_Triple(Triple $visitee, $depth) {
    if ($depth) {
      $this->sql .= '(';
    }
    else {
      $this->joins = array();
    }

    $visitee->leftOperand->accept($this, $depth+1);
    $visitee->xBoolean->accept($this, $depth+1);
    $visitee->rightOperand->accept($this, $depth+1);

    if ($depth) {
      $this->sql .= ')';
    }
    else {
      $this->sql = $this->complete($this->joins, $this->sql);
    }
  }

  /**
   * Description needed.
   */
  protected function visit_searchClause(searchClause $visitee, $depth) {
    if ($depth) {
      $this->sql .= '(';
    }
    else {
      $this->joins = array();
    }

    $this->sql .= $this->despatchIRT($visitee->index, $visitee->relation, $visitee->term);

    if ($depth) {
      $this->sql .= ')';
    }
    else {
      $this->sql = $this->complete($this->joins, $this->sql);
    }
  }

  /**
   * Build the final SELECT function
   *
   * Rally:US161:DE124 -- fixed the stupid MySQL problem, have to do it in two parts
   *
   * NOTE: The rule is: *all* content is visible to everyone, LA Consultant private
   * info is dealt with elsewhere therefore can be ignored here. So, status=1 always.
   * (There *is* unpublished content but that is archived and seen by no one.)
   *
   * @todo Some content-type-specific business rules have been removed, as
   *    which content types will be used, and how, is an unknown.
   *    Potential pain point.
   */
  private function complete($joins, $where) {
    static $temp_table_flag = FALSE;

    $taggable_types = cql_nodetypes();


    $joins = is_array($joins) ? implode(' ', array_keys($joins)) : '';
    $qry = 'SELECT DISTINCT(n.nid)
        FROM {node} n ' . $joins . '
        WHERE ' . $where;
    return $qry;
  }

  /**
   * Description needed.
   */
  protected function visit_xBoolean(xBoolean $visitee, $depth) {
    $op = drupal_strtolower($visitee);
    if ($op == 'prox') {
      $op = 'and';
    }
    $this->sql .= ' ' . drupal_strtoupper($op) . ' ';
  }

  /**
   * Description needed.
   */
  protected function visit_Index(Index $visitee, $depth) {
    $this->sql .=  (defaultServerIndex == $visitee) ? ' ns.body' : (string) $visitee;
  }

  /**
   * Description needed.
   */
  protected function visit_Relation(Relation $visitee, $depth) {
    $index = (string) $visitee;
    $prefix = $visitee->prefix;
    $value = $visitee->value;
    $is_default = (defaultServerRelation == $visitee) || (($prefix == serverContextSet) && ($value=='='));

    $this->sql .=  $is_default ? ' LIKE ' : $index;
  }

  /**
   * Description needed.
   */
  protected function visit_Term(Term $visitee, $depth) {
    $this->sql .= '"%' .  (string) $visitee . '%" ';
  }

  /**
   * Description needed.
   */
  protected function visit_PrefixableObject(PrefixableObject $visitee, $depth) {
    ;
  }

  /**
   * Description needed.
   */
  protected function visit_ModifiableObject(ModifiableObject $visitee, $depth) {
    ;
  }

  /**
   * Description needed.
   */
  protected function visit_ModifierClause(ModifierClause $visitee, $depth) {
    ;
  }

  /**
   * Description needed.
   */
  public function __toString() {
    return $this->sql;
  }
}
