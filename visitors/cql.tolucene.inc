<?php
/**
 * @file
 * @brief
 * Visit all the nodes in a parsed CQL structure and
 * generate the MySQL WHERE clause output for that structure
 *
 * @details
 *
 * @package CQL
 * @subpackage
 * @author
 */

_cql_load_visitor_file();

class cqlVisitorGenLucene extends cqlVisitor {
  private $qry = '' ;

  /**
   * Description needed.
   */
  public function __construct() {
    parent::__construct('Lucene');
  }

  /**
   * Description needed.
   */
  protected function visit_Triple(Triple $visitee, $depth) {
    if ($depth) {
      $this->qry .= '(';
    }
    else {
      $this->joins = array();
    }

    $visitee->leftOperand->accept($this, $depth+1);
    $visitee->xBoolean->accept($this, $depth+1);
    $visitee->rightOperand->accept($this, $depth+1);

    if ($depth) {
      $this->qry .= ')';
    }
    else {
      $this->qry = $this->complete($this->qry);
    }
  }

  /**
   * Description needed.
   */
  protected function visit_searchClause(searchClause $visitee, $depth) {
    if ($depth) {
      $this->qry .= '(';
    }

    $this->qry .= $this->despatchIRT($visitee->index, $visitee->relation, $visitee->term);

    if ($depth) {
      $this->qry .= ')';
    }
    else {
      $this->qry = $this->complete($this->qry);
    }
  }

  /**
   * Description needed.
   */
  protected function visit_xBoolean(xBoolean $visitee, $depth) {
    $op = (string) $visitee;
    if ($op == 'prox') {
      $op = 'and';
    }
    $this->qry .= ' ' . drupal_strtoupper($op) . ' ';
  }

  /**
   * Description needed.
   */
  protected function visit_Index(Index $visitee, $depth) {
    ;
  }

  /**
   * Description needed.
   */
  protected function visit_Relation(Relation $visitee, $depth) {
    ;
  }

  /**
   * Description needed.
   */
  protected function visit_Term(Term $visitee, $depth) {
    ;
  }

  /**
   * Description needed.
   */
  protected function visit_PrefixableObject(PrefixableObject $visitee, $depth) {
    ;
  }

  /**
   * Description needed.
   */
  protected function visit_ModifiableObject(ModifiableObject $visitee, $depth) {
    ;
  }

  /**
   * Description needed.
   */
  protected function visit_ModifierClause(ModifierClause $visitee, $depth) {
    ;
  }

  /**
   * Description needed.
   */
  protected function complete($qry) {
    return $qry;
  }

  /**
   * Description needed.
   */
  public function __toString() {
    return $this->qry;
  }
}

