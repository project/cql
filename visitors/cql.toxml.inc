<?php

/**
 * @file
 * @brief
 * Visit all the nodes in a parsed CQL structure and
 * generate the XML output for that structure
 *
 * @details
 *
 * @package CQL
 * @subpackage
 * @author
 */
_cql_load_visitor_file();

class cqlVisitorGenXML extends cqlVisitor {
  const XCQLIndent = '    ' ;
  private $xml;

  /**
   * Description needed.
   */
  public function __construct() {
    parent::__construct('XML');
  }

  /**
   * Description needed.
   */
  protected function visit_Triple(Triple $visitee, $depth) {
    $space = str_repeat(cqlVisitorGenXML::XCQLIndent, $depth);
    $space1 = $space . cqlVisitorGenXML::XCQLIndent;

    $this->xml .= sprintf("%s<triple>\n", $space);

    $this->visit_PrefixableObject($visitee, $depth+1);

    $visitee->xBoolean->accept($this, $depth+1);

    $this->xml .= sprintf("%s<leftOperand>\n", $space1);
    $visitee->leftOperand->accept($this, $depth+2);
    $this->xml .= sprintf("%s</leftOperand>\n", $space1);

    $this->xml .= sprintf("%s<rightOperand>\n", $space1);
    $visitee->rightOperand->accept($this, $depth+2);
    $this->xml .= sprintf("%s</rightOperand>\n", $space1);

    $this->xml .= sprintf("%s</triple>\n", $space);
  }

  /**
   * Description needed.
   */
  protected function visit_xBoolean(xBoolean $visitee, $depth) {
    $space = str_repeat(cqlVisitorGenXML::XCQLIndent, $depth);

    $this->xml .= sprintf("%s<boolean>\n", $space);
    $this->xml .= sprintf("%s<value>%s</value>\n", $space, $visitee->value);
    $this->visit_ModifiableObject($visitee, $depth+1);
    $this->xml .= sprintf("%s</boolean>\n", $space);
  }

  /**
   * Description needed.
   */
  protected function visit_searchClause(searchClause $visitee, $depth) {
    $space = str_repeat(cqlVisitorGenXML::XCQLIndent, $depth);

    $this->xml .= sprintf("%s<searchClause>\n", $space);
    $this->visit_PrefixableObject($visitee, $depth+1);
    $visitee->index->accept($this, $depth+1);
    $visitee->relation->accept($this, $depth+1);
    $visitee->term->accept($this, $depth+1);
    $this->xml .= sprintf("%s</searchClause>\n", $space);
  }

  /**
   * Description needed.
   */
  protected function visit_Index(Index $visitee, $depth) {
    $space = str_repeat(cqlVisitorGenXML::XCQLIndent, $depth);

    $this->xml .= sprintf("%s<index>%s</index>\n", $space, (string) $visitee);
  }

  /**
   * Description needed.
   */
  protected function visit_Relation(Relation $visitee, $depth) {
    $space = str_repeat(cqlVisitorGenXML::XCQLIndent, $depth);
    $space1 = $space . cqlVisitorGenXML::XCQLIndent;

    $this->xml .= sprintf("%s<relation>\n", $space);
    $this->xml .= sprintf("%s<value>%s</value>\n", $space1, addslashes((string) $visitee));
    $this->visit_ModifiableObject($visitee, $depth+1);
    $this->xml .= sprintf("%s</relation>\n", $space);
  }

  /**
   * Description needed.
   */
  protected function visit_Term(Term $visitee, $depth) {
    $space = str_repeat(cqlVisitorGenXML::XCQLIndent, $depth);

    $this->xml .= sprintf("%s<term>%s</term>\n", $space, (string) $visitee);
  }

  /**
   * Description needed.
   */
  protected function visit_PrefixableObject(PrefixableObject $visitee, $depth) {
    if (!empty($visitee->prefixes)) {
      $space = str_repeat(cqlVisitorGenXML::XCQLIndent, $depth);
      $space1 = $space . cqlVisitorGenXML::XCQLIndent;
      $space2 = $space1 . cqlVisitorGenXML::XCQLIndent;

      $str = "%s<prefix>\n%s<name>%s</name>\n%s<identifier>%s</identifier>\n%s</prefix>\n" ;
      $this->xml .= sprintf("%s<prefixes>\n", $space);
      foreach ($visitee->prefixes as $name => $identifier) {
        $this->xml .= sprintf($str, $space1, $space2, $name, $space2, addslashes($identifier), $space1);
      }
      $this->xml .= sprintf("%s</prefixes>\n", $space);
    }
  }

  /**
   * Description needed.
   */
  protected function visit_ModifiableObject(ModifiableObject $visitee, $depth) {
    if (!empty($visitee->modifiers)) {
      $space = str_repeat(cqlVisitorGenXML::XCQLIndent, $depth);

      $this->xml .= sprintf("%s<modifiers>\n", $space);
      foreach ($visitee->modifiers as $modifier) {
        $this->xml .= $modifier->accept($this, $depth+1);
      }
      $this->xml .= sprintf("%s</modifiers>\n", $space);
    }
  }

  /**
   * Description needed.
   */
  protected function visit_ModifierClause(ModifierClause $visitee, $depth) {
    $space = str_repeat(cqlVisitorGenXML::XCQLIndent, $depth);
    $space1 = $space . cqlVisitorGenXML::XCQLIndent;

    $this->xml .= $space . "<modifier>\n" ;
    $this->xml .= $space1 . '<type>' . addslashes($visitee->type) . "</type>\n" ;
    if ($this->value) {
      $this->xml .= $space1 . '<comparison>' . addslashes($visitee->comparison) . "</comparison>\n" ;
      $this->xml .= $space1 . '<value>' . addslashes($visitee->value) . "</value>\n" ;
    }
    $this->xml .= $space . "</modifier>\n" ;
  }

  /**
   * Description needed.
   */
  public function __toString() {
    return $this->xml;
  }
}
