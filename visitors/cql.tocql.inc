<?php

/**
 * @file
 * @brief
 * Visit all the nodes in a parsed CQL structure and
 * generate the CQL output for that structure.
 *
 * @details
 *
 * @package 	CQL
 * @subpackage	visitors
 * @author
 */
_cql_load_visitor_file();

class cqlVisitorGenCQL extends cqlVisitor {
  private $cql = '';

  /**
   * Description needed.
   */
  public function __construct() {
    parent::__construct('CQL');
  }

  /**
   * Description needed.
   */
  protected function visit_Triple(Triple $visitee, $depth) {

    if ($depth) {
      $this->cql .= ' (';
    }
    $this->cql .= ' ' . $visitee->leftOperand->accept($this, $depth+1);
    $this->cql .= ' ' . $visitee->xBoolean->accept($this, $depth+1);
    $this->cql .= ' ' . $visitee->rightOperand->accept($this, $depth+1);
    if ($prefs = $this->visit_PrefixableObject($visitee, $depth+1) ) {
      $this->cql .= $prefs . ' ' . $this->cql;
    }
    if ($depth) {
      $this->cql .= ') ';
    }
  }

  /**
   * Description needed.
   */
  protected function visit_searchClause(searchClause $visitee, $depth) {

    if ($depth) {
      $this->cql .= ' (';
    }
    $this->cql .= ' ' . $visitee->index->accept($this, $depth+1);
    $this->cql .= ' ' . $visitee->relation->accept($this, $depth+1);
    $this->cql .= ' ' . $visitee->term->accept($this, $depth+1);
    if ($prefs = $this->visit_PrefixableObject($visitee, $depth+1) ) {
      $this->cql .= $prefs . ' ' . $this->cql;
    }
    if ($depth) {
      $this->cql .= ') ';
    }
  }

  /**
   * Description needed.
   */
  protected function visit_xBoolean(xBoolean $visitee, $depth) {
    $this->cql .= (string) $visitee;
    if ($mods = $this->visit_ModifiableObject($visitee, $depth + 1)) {
      $this->cql .= $mods;
    }
  }

  /**
   * Description needed.
   */
  protected function visit_Index(Index $visitee, $depth) {
    $this->cql .= (string) $visitee;
  }

  /**
   * Description needed.
   */
  protected function visit_Relation(Relation $visitee, $depth) {
    $this->cql .= (string) $visitee;
    if ($mods = $this->visit_ModifiableObject($visitee, $depth + 1)) {
      $this->cql .= $mods;
    }
  }

  /**
   * Description needed.
   */
  protected function visit_Term(Term $visitee, $depth) {
    $this->cql .= (string) $visitee;
  }

  /**
   * Description needed.
   */
  protected function visit_PrefixableObject(PrefixableObject $visitee, $depth) {
    if (!empty($visitee->prefixes)) {
      foreach ($visitee->prefixes as $name => $identifier) {
        if ($name) {
          $this->cql .= sprintf(' >%s="%s"', $name, $identifier);
        }
        else {
          $this->cql .= sprintf(' >%s="%s"', $identifier);
        }
      }
    }
  }

  /**
   * Description needed.
   */
  protected function visit_ModifiableObject(ModifiableObject $visitee, $depth) {
    $sep = '/';
    if (!empty($visitee->modifiers)) {
      foreach ($visitee->modifiers as $modifier) {
        $this->cql .= $sep . $this->visit_ModifierClause($modifier, $depth);
      }
    }
  }

  /**
   * Description needed.
   */
  protected function visit_ModifierClause(ModifierClause $visitee, $depth) {
    $this->cql .= (string) $visitee;
  }

  /**
   * Description needed.
   */
  public function __toString() {
    return $this->cql;
  }
}

