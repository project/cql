<?php

/**
 * @file
 * @brief
 * Implement Common Query Language in a Drupal environment.
 *
 * Class that looks after executing queries then storing and
 * delivering results if possible the results are cached.
 *
 * @update
 * 3 Sep 2008 - Enhanced to write all data to temp table.
 *
 * @package 	CQL
 * @subpackage	executors
 * @author
 */

module_load_include('inc', 'cql', 'ns.lucene');
abstract class cqlExecutor {
  const MYSQL = 'MySQL';
  const LUCENE = 'Lucene';

  static private $instances = array();

  /**
   * Description needed.
   */
  static public function factory($cql, $extras = array('fields' => array('nid')), $engine = self::LUCENE) {

    $cql = trim(preg_replace('/\s\s+/', ' ', $cql));
    $cid = self::make_cid($cql, $engine);

    if (!array_key_exists($cid, self::$instances)) {
      switch ($engine) {
        case self::MYSQL:
          $instance = new cqlExecutorMySQL($cid, $cql, $extras['temptable']);
          break;
        case self::LUCENE:
          $instance = new cqlExecutorLucene($cid, $cql, $extras['fields'], $extras['sort']);
          break;
      }
      self::$instances[$cid] = $instance;
      $instance->get_results();
    }
    else {
      self::$instances[$cid]->set_fields($extras['fields'], $extras['sort']);
      self::$instances[$cid]->get_results();
    }
    return self::$instances[$cid];
  }

  /**
   * Description needed.
   */
  static public function stock($cid) {
    $instance = NULL;
    if (array_key_exists($cid, self::$instances)) {
      $instance = self::$instances[$cid];
    }
    return $instance;
  }

  /**
   * Description needed.
   */
  static public function checkCQL($cql) {
    if ($cql == '') {
      return TRUE;
    }

    _cql_load_parser_file();
    $parsed = TRUE;
    try {
      $parser = new CQLParser($cql);
      $parsed = $parser->parsequery();
      if ($parsed === FALSE) {
        throw new Exception('Parsing of CQL query failed - ' . $cql);
      }
    }
    catch (Exception $e) {
      watchdog('cqlExecutor', '%message', array('%message' => $e->getMessage() . ' ## ' . $cql)); 
      watchdog('cqlExecutor', '%tracestring', array('%tracestring' => $e->getTraceAsString()));
      $parsed = FALSE;
      throw $e;
    }
    return $parsed;
  }

  /**
   * Description needed.
   */
  static public function convertToLucene($cql) {
    if ($cql == '') {
      return '';
    }
    $query = FALSE;
    try {
      $parsed = self::checkCQL($cql);

      _cql_load_lucenevisitor_file();
      $visitor = new cqlVisitorGenLucene();
      $parsed->accept($visitor);
      $query = (string) $visitor;
    }
    catch (Exception $e) {
      watchdog('cqlExecutor', '%message', array('%message' => $e->getMessage() . ' ## ' . $cql)); 
      watchdog('cqlExecutor', '%tracestring', array('%tracestring' => $e->getTraceAsString()));
      $parsed = FALSE;
      throw $e;
    }
    return $query;
  }

  /**
   * Description needed.
   */
  static public function make_cid($cql, $engine = self::LUCENE) {
    $query = drupal_strtolower(preg_replace('/\s\s+/', ' ', $cql));
    return str_replace('0', 'g', md5($cql . $engine));
  }

  protected $engine = NULL;
  protected $nousers = TRUE;
  protected $cid;
  protected $cql;
  protected $query;

  protected $total = 0;

  /**
   * Description needed.
   */
  protected function __construct($cid, $cql) {
    _cql_load_parser_file();
    $this->cid = $cid;
    $this->cql = $cql;

    try {
      $parser = new CQLParser($cql);
      $parsed = $parser->parsequery();
      if ($parsed === FALSE) {
        throw new Exception('Parsing of CQL query failed - ' . $cql);
      }

      $visitor = $this->visitor();
      $parsed->accept($visitor);
      $this->query = (string) $visitor;
    }
    catch (Exception $e) {
      watchdog('cqlExecutor', '%message', array('%message' => $e->getMessage() . ' ## ' . $cql . ' ## ' . $this->query));
      watchdog('cqlExecutor', '%tracestring', array('%tracestring' => $e->getTraceAsString()));
      throw $e;
    }
  }

  /*
   * SRU assumes the records start at 1, not 0
   */

  /**
   * Description needed.
   */
  abstract protected function fetch($start = 1, $limit = 5000);

  /**
   * Description needed.
   */
  abstract protected function get_results();

  /**
   * Description needed.
   */
  abstract protected function visitor();

  /**
   * Description needed.
   */
  abstract protected function set_fields($fields = array());

  /**
   * Description needed.
   */
  public function __get($var) {
    $val = NULL;
    switch ($var) {
      case 'query':
        $val = $this->cql;
        break;
      case 'lucene':
        $val = $this->query;
        break;
    }
    return $val;
  }

  /**
   * Description needed.
   */
  public function cid() {
    return $this->cid;
  }

  /**
   * Description needed.
   */
  public function total() {
    return $this->total;
  }

  /**
   * This static method forces the given nids into the given temporary table
   * this is needed as a temp bug fix because AJAX search results is essentially
   * a new page and cannot access a previous temporary file. It does it in
   * blocks of 100 nids at a time to keep the calls down. This is a temporary fix.
   *
   * Added the 'z' in front of the temptable because if it's an MD5 it might start
   * with a digit, which is illegal.
   */
  static public function force_temp($nids, $temptable) {
    if ($temptable) {
      global $db_prefix;
      $db_prefix[$temptable] = '';
      db_query('DROP TABLE IF EXISTS %s', $temptable);
      db_query('CREATE TEMPORARY TABLE %s (nid INT(11), PRIMARY KEY (nid))', $temptable);

      $offset = 0;
      $length = 100;
      while ($niddles = array_slice($nids, $offset, $length)) {
        $vals = trim(str_repeat('(%d),', count($niddles)), ',');
        $qry = 'INSERT INTO ' . $temptable . ' (nid) VALUES ' . $vals;
        db_query($qry, $niddles);
        $offset += $length;
      }
    }
  }
}

class cqlExecutorMySQL extends cqlExecutor {

  protected $nids = array();
  protected $temptable = FALSE;

  /**
   * Description needed.
   */
  protected function __construct($cid, $query, $temptable = NULL) {
    $this->engine = self::MYSQL;
    parent::__construct($cid, $query);
    if ($temptable) {
      $this->temptable = $temptable;
    }

    if (module_exists('cqlcache') && !$this->temptable) {
      $this->nids = cqlcache_get_results($cid);
      $this->total = count($this->nids);
    }
    if (empty($this->nids)) {
      $this->get_results();
      if (module_exists('cqlcache')) {
        cqlcache_set_results($this->cid, $this->nids); // auto-sets count
      }
    }
  }

  /**
   * Description needed.
   */
  protected function visitor() {
    _cql_load_mysqlvisitor_file();
    return new cqlVisitorGenMySQL();
  }

  /**
   * Description needed.
   */
  protected function get_results() {
    if ($this->temptable) {
      db_query_temporary($this->query, $this->temptable);
      $result = db_query('SELECT * FROM %s', $this->temptable);
    }
    else {
      $result = db_query($this->query);
    }

    while ($nid = db_fetch_object($result)) {
      $this->nids[$nid->nid] = $nid->nid;
    }
    $this->total = count($this->nids);
  }

  /**
   * Description needed.
   */
  public function fetch($start = 1, $limit = 5000) {
    return array_slice($this->nids, $start-1, $limit);
  }

  /**
   * Description needed.
   */
  protected function set_fields($fields = array()) {
    ;
  }
}

class cqlExecutorLucene extends cqlExecutor {
  protected $fetch;
  protected $fields;

  /**
   * Description needed.
   */
  protected function __construct($cid, $query, $fields, $sort = NULL) {
    $this->engine = self::LUCENE;
    $this->fields = $fields;
    $this->sort = $sort;
    parent::__construct($cid, $query);
  }

  /**
   * Description needed.
   */
  protected function visitor() {
    _cql_load_lucenevisitor_file();
    return new cqlVisitorGenLucene();
  }

  /**
   * Description needed.
   */
  protected function get_results() {
    $this->fetch = ns_lucene::query($this->query, $this->fields, $this->sort);
    $this->total = $this->fetch->count();
  }

  /**
   * Description needed.
   */
  public function fetch($start = 1, $limit = 5000) {
    if ($start > 0) {
      $start -= 1;
    }
    $this->fetch->set($start, $limit);
    $this->fetch->run();
    return $this->fetch->results();
  }

  /**
   * Description needed.
   */
  public function raw($start = NULL, $limit = NULL, $fields = NULL) {
    if ($start > 0) {
      $start -= 1;
    }
    $this->fetch->set($start, $limit, $fields);
    $this->fetch->run();
    return $this->fetch->raw();
  }

  /**
   * Description needed.
   */
  protected function set_fields($fields = array(), $sort = NULL) {
    $this->fields = $fields;
    $this->sort = $sort;
  }

  /**
   * Description needed.
   */
  static public function compressTerms($terms) {
    $grouped = array();
    switch (count($terms)) {
      case 0:
        break;
      case 1:
        $grouped[] = (string) $terms[0];
        break;
      default:
        $terms = array_flip($terms);
        $terms = array_flip($terms);
        sort($terms);
        $last = $range = array_shift($terms);
        if (count($terms)) {
          foreach ($terms as $term) {
            if ($range==$term-1) {
              $range = $term;
            }
            else {
              if ($last == $range) {
                $grouped[] = (string) $last;
              }
              else {
                $grouped[] = "[{$last} TO {$range}]" ;
              }
              $last = $range = $term;
            }
          }
          if ($last == $range) {
            $grouped[] = (string) $last;
          }
          else {
            $grouped[] = "[{$last} TO {$range}]" ;
          }
        }
        break;
    }
    return $grouped;
  }
}
