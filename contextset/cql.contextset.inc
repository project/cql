<?php

/**
 * @file
 *
 * @brief
 * Implement Common Query Language in a Drupal environment.
 *
 * This include file defines the cqlContextSet class that extends contextSet.
 *
 * @package 	CQL
 * @subpackage
 * @author
 */

class cqlContextSet extends contextSet {

  /**
   * Description needed.
   */
  protected function __construct($data) {
    parent::__construct('cql', 'info:srw/cql-context-set/1/cql-v1.2', $data);
  }

  /**
   * All the data initialisation routines
   */
  protected function indexes() {
    return array(
            'anyIndexes' => NULL,
            'resultSetId' => NULL,
            'allRecords' => NULL,
            'allIndexes' => NULL,
            'keywords' => NULL,
            'serverChoice' => 'anyIndexes',
            'anywhere' => 'allIndexes',
            );
  }

  /**
   * Description needed.
   */
  protected function rels() {
    return array(
            '=' => 'eq',
            '==' => 'eeq',
            '<>' => 'ne',
            '<' => 'lt',
            '>' => 'gt',
            '<=' => 'le',
            '>=' => 'ge',
            'adj' => NULL,
            'all' => NULL,
            'any' => NULL,
            'within' => NULL,
            'encloses' => NULL,
    );
  }

  /**
   * Description needed.
   */
  protected function rmods() {
    return array(
      'stem',
      'relevant',
      'phonetic',
      'fuzzy',
      'partial',
      'ignoreCase',
      'respectCase',
      'ignoreAccents',
      'respectAccents',
      'locale',
      'word',
      'string',
      'isoDate',
      'number',
      'uri',
      'oid',
      'unmasked',
      'substring',
      'regexp',
    );
  }

  /**
   * Description needed.
   */
  protected function bools() {
    return array(
      'and',
      'or',
      'not',
      'prox',
    );
  }

  /**
   * Description needed.
   */
  protected function bmods() {
    return array(
      'unordered',
      'distance',
      'unit',
      'ordered',
    );
  }

  /**
   * Description needed.
   */
  protected function irtCheck_resultsetid($i, $r, $t) {
    list($rcs, $rrel) = explode('.', $r);
    if (($rcs != $this->name) && ($rrel != '=')) {
      throw new Exception(t('resultSetId must be followed by "="'));
    }
  }
}
