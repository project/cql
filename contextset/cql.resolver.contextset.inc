<?php

/**
 * @file
 * @brief
 * The set of CQL IRT resolvers.
 *
 * Defines classes that extend irtResolver.
 *
 * @package 	CQL
 * @subpackage	resolvers
 * @author
 */

_cql_load_resolver_file();

class cqlIrtResolverMySQL extends irtResolver {

  /**
   * Description needed.
   */
  public function resolveIndex(&$irtPack, $idx, $i, &$r, $t, &$joins) {
    switch ($idx) {
      case 'anyindexes':
      case 'serverchoice':
        $ns = contextSet::get_context_set('ns');
        $irtPack->add_index('body');
        $joins[$ns->dbjoin] = NULL;
        break;
      case 'allrecords':
        $irtPack->add_output('1=1');
        $irtPack->set_done();
        break;
      default:
        throw new Exception(t('The CQL index "!index" is not implemented', array('!index' => $idx)));
        break;
    }
  }

  /**
   * Description needed.
   */
  public function resolveRelation(&$irtPack, $rel, $i, $r, $t) {
    $match = $this->stripquotes((string) $t);
    switch ($rel) {
      case '=':
        $op = $this->relate('OR', $irtPack->get_indexes(), 'LIKE', "%%{$match}%%");
        break;
      case '<>':
        $op = $this->relate('AND', $irtPack->get_indexes(), 'NOT LIKE', "%%{$match}%%");
        break;
      case '>':
      case '>=':
      case '<':
      case '<=':
        $op = $this->relate('AND', $irtPack->get_indexes(), $rel, $match);
        break;
      default:
        throw new Exception(t('The CQL relation "!rel" is not implemented', array('!rel' => $rel)));
        break;
    }
    $irtPack->add_output($op);
    $irtPack->set_done();
  }

  /**
   * Description needed.
   */
  private function relate($operator, $indexes, $func, $match) {
    $op = '' ;
    if (!empty($indexes)) {
      $first = TRUE;
      foreach ($indexes as $index) {
        if (!$first) {
          $op .= $operator;
        }
        else {
          $first = FALSE;
        }
        $op .= "({$index} {$func} '{$match}')" ;
      }
    }
    return $op;
  }

}

/**
 * Resolver for Apache Solr
 */
class cqlIrtResolverLucene extends irtResolver {

  /**
   * Description needed.
   */
  public function resolveIndex(&$irtPack, $idx, $i, &$r, $t, &$joins) {
    switch ($idx) {
      case 'anyindexes':
      case 'serverchoice':
        $irtPack->add_index('title');
        break;
      case 'allrecords':
        $irtPack->add_output('*:*');
        $irtPack->set_done();
        break;
      default:
        throw new Exception(t('The CQL index "!index" is not implemented', array('!index' => $idx)));
        break;
    }

  }

  /**
   * Description needed.
   */
  public function resolveRelation(&$irtPack, $rel, $i, $r, $t) {
    $match = (string) $t;
    switch ($rel) {
      case '=':
        $op = $this->relate($match, $irtPack->get_indexes(), '');
        break;
      case '<>':
        $op = $this->relate($match, $irtPack->get_indexes(), '-');
        break;
      case '>':
      case '>=':
      case '<':
      case '<=':
      default:
        throw new Exception(t('The CQL relation "!rel" is not implemented in Lucene', array('!rel' => $rel)));
        break;
    }
    $irtPack->add_output($op);
    $irtPack->set_done();
  }

  /**
   * Description needed.
   */
  private function relate($match, $indexes, $op = '+') {
    $out = '' ;
    if (!empty($indexes)) {
      foreach ($indexes as $index) {
        $out .= ' ' . $index . ':' . $op . $match;
      }
    }
    return trim($out);
  }
}
