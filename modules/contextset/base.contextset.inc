<?php

/**
 * @file
 * @brief
 * Manages the context set modules for CQL.
 *
 * An Abstract parent class of all context sets that
 * are recognised by this implementation of CQL.
 *
 * It's main functions are to recognise tokens for the
 * parser and determine whether they are relations or booleans.*
 *
 * Plus when generating output they produce the necessary
 * output for the type of visitor doing the processing.
 *
 * We use the 'interface' structure because PHP doesn't
 * permit multiple inheritance
 *
 * @package 	contextset
 * @subpackage
 * @author
 */

abstract class contextset {
  protected $name = NULL;
  protected $ident = NULL;
  protected $join = NULL;
  protected $abbv = NULL;
  protected $indices = NULL;
  protected $file = NULL;

  // -- these contain the context set keywords

  protected $indexes = array();
  protected $rels = array();
  protected $rmods = array();
  protected $bools = array();
  protected $bmods = array();

  // -- these are the context set keyword attributes (keyed by keyword)

  protected $kw_idxs = array();
  protected $kw_rels = array();

  /**
   * Factory to produce the required context set objects as *singletons*
   * also sets up the default context set (the first one added) and the
   * relevant default index and default relation
   */

  protected static $contextsets = array();
  protected static $defcontextset = NULL;
  protected static $defcsi = NULL;
  protected static $defcsr = NULL;

  public static function make($contextset, $contextsetdata) {
    if (!array_key_exists($contextset, contextset::$contextsets)) {
      require_once($contextsetdata['file']);
      $contextsetclass = $contextset . 'ContextSet';
      $cs = contextset::$contextsets[$contextset] = new $contextsetclass($contextsetdata);
      if (!contextset::$defcontextset) {
        contextset::$defcontextset = $contextset;
        contextset::$defcsi = $cs->indexes[0];
        contextset::$defcsr = $cs->rels[0];
      }
    }
    return contextset::$contextsets[$contextset];
  }

  public static function get_cs() { return contextset::$defcontextset; }
  public static function get_csi() { return contextset::$defcsi; }
  public static function get_csr() { return contextset::$defcsr; }

  public static function get_prefix_set() {
    $prefixes = array();
    if (!empty(contextset::$contextsets)) {
      foreach (contextset::$contextsets as $name => $contextset) {
        $prefixes[$name] = $contextset->ident;
      }
    }
    return $prefixes;
  }

  public static function get_context_set($name) {
    return contextset::$contextsets[$name];
  }

  protected function __construct($name, $ident, $data) {
    $this->name = $name;
    $this->ident = $ident;
    $this->dbjoin = $data['join'];
    $this->abbv = $data['abbv'];
    $this->indices = $data['indices'];
    $this->file = $data['file'];

    $this->add_keywords($this->indexes(), $this->indexes, $this->kw_idxs);
    $this->add_keywords($this->rels(), $this->rels, $this->kw_rels);

    $this->rmods = $this->rmods();
    $this->bools = $this->bools();
    $this->bmods = $this->bmods();
  }

  private function add_keywords($src, &$list, &$kwds) {
    if (!empty($src)) {
      foreach ($src as $name => $func) {
        $list[] = $name;
        if (!$func) {
          $func = $name;
        }
        $kwds[$name] = $func; // $this->name . ':' . $func;
      }
    }
  }


  /**
   * All the data initialisation routines
   */

  abstract protected function indexes();
  abstract protected function rels();
  abstract protected function rmods();
  abstract protected function bools();
  abstract protected function bmods();

  final public function __get($var) {
    switch ($var) {
      case 'dbjoin':
      case 'abbv':
        $val = $this->$var;
        break;
    }
    return $val;
  }

  /**
   * Routines which are used to check whether the token is recognised
   */

  // this group are for each instance, if the token matches
  // return the context set to which it belongs

  final protected function is_one($token, &$names) {
    return in_array($token, $names) ? $this->name : FALSE;
  }

  protected function is_index($token) {
    return $this->is_one($token, $this->indexes);
  }

  protected function is_boolean($token) {
    return $this->is_one($token, $this->bools);
  }

  protected function is_boolmod($token) {
    return $this->is_one($token, $this->bmods);
  }

  protected function is_relation($token) {
    return $this->is_one($token, $this->rels);
  }

  protected function is_relmod($token) {
    return $this->is_one($token, $this->rmods);
  }

  // while this group are static and search all the instances

  static protected function is_one_of_all($token, $func) {
    $found = FALSE;
    if (!empty(contextset::$contextsets)) {
      foreach (contextset::$contextsets as $contextset) {
        if ($found = $contextset->$func($token)) {
          break;
        }
      }
    }
    return $found;
  }

  static public function find_index($token) {
    return contextset::is_one_of_all($token, 'is_index');
  }

  static public function find_boolean($token) {
    return contextset::is_one_of_all($token, 'is_boolean');
  }

  static public function find_boolmod($token) {
    return contextset::is_one_of_all($token, 'is_boolmod');
  }

  static public function find_relation($token) {
    return contextset::is_one_of_all($token, 'is_relation');
  }

  static public function find_relmod($token) {
    return contextset::is_one_of_all($token, 'is_relmod');
  }

  /**
   * Routines to put together the information for the parser
   */

  static public function collect_booleans() {
    $bools = array();
    if (!empty(contextset::$contextsets)) {
      foreach (contextset::$contextsets as $contextset) {
        $bools = array_merge($bools, $contextset->bools);
      }
    }
    return $bools;
  }

  static public function collect_relations() {
    $bools = array();
    if (!empty(contextset::$contextsets)) {
      foreach (contextset::$contextsets as $contextset) {
        $bools = array_merge($bools, $contextset->rels);
      }
    }
    return $bools;
  }

  /**
   * Check that the supplied IRT is valid
   *
   * @param $i
   * The index object
   *
   * @param $r
   * The relation object
   *
   * @param $t
   * The term object
   *
   * If there's an error it throws an exception
   *
   */

  static public function check_irt_syntax($i, $r, $t) {
    list($cs, $idx) = explode('.', $i);
    $cs = contextset::$contextsets[$cs];
    $cs->irt_syntax_check_index($idx, $i, $r, $t);    // FIXME: ( ! ) Fatal error: Call to a member function irt_syntax_check_index() on a non-object sites/all/modules/cql/modules/contextset/contextset.base.inc on line 250


    list($cs, $rel) = explode('.', $r);
    $cs = contextset::$contextsets[$cs];
    $cs->irt_syntax_check_relation($rel, $i, $r, $t);
  }

  private function irt_syntax_check_index($idx, $i, $r, $t) {
    $func = 'irtcheck_' . $this->kw_idxs[$idx];
    if (method_exists($this, $func)) {
      $this->$func($i, $r, $t);
    }
  }

  private function irt_syntax_check_relation($rel, $i, $r, $t) {
    $func = 'irtcheck_' . $this->kw_rels[$rel];
    if (method_exists($this, $func)) {
      $this->$func($i, $r, $t);
    }
  }

  static public function fetch_resolvers($type) {
    $resolvers = array();
    $tail = 'Irtresolver' . $type;
    if (!empty(contextset::$contextsets)) {
      foreach (contextset::$contextsets as $cs => $contextset) {
        $parser_file = str_replace('contextset.inc', 'resolver.contextset.inc', $contextset->file); // load parser file
        include_once $parser_file;
        $class = $cs . $tail;
        if (class_exists($class, FALSE)) {
          $resolvers[$cs] = new $class();
        }
      }
    }
    return $resolvers;
  }

  /**
   * Fetch all of the real indexes for this context and its "join"
   */

  static public function all_indexes(&$joins) {
    $indexes = array();
    if (!emptycontextset::$contextsets) {
      foreach (contextset::$contextsets as $cs) {
        $keys = $cs->key_indexes();
        if (count($keys)) {
          $indexes = array_merge($indexes, $keys);
          $joins[$cs->dbjoin] = NULL;
        }
      }
    }
    return $indexes;
  }

  private function key_indexes() {
    $keys = array();
    if (!empty($this->indices)) {
      foreach ($this->indices as $idx) {
        $keys[$this->abbv . '.' . $idx] = NULL;
      }
    }
    return $keys;
  }

  /**
   * Fetch all context sets and their idents (used in SRU's explainResponse)
   */

  static public function all_set_info() {
    $sets = array();
    if (!empty(contextset::$contextsets)) {
      foreach (contextset::$contextsets as $cs) {
        $sets[$cs->name] = $cs->ident;
      }
    }
    return $sets;
  }

  /**
   * Fetch all context set indexes (used in SRU's explainResponse)
   */

  static public function all_index_info() {
    $indexes = array();
    if (!empty(contextset::$contextsets)) {
      foreach (contextset::$contextsets as $cs) {
        foreach ($cs->indexes as $name => $stuff) {
          $indexes[] = array(
                        'title' => $stuff,
                        'name' => $stuff,
                        'set' => $cs->name
          );
        }
      }
    }
    return $indexes;
  }

}

