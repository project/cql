<?php

/**
 * @file
 * @brief
 * Handles caching for CQL queries and results
 *
 * @package 	cql-cache
 * @subpackage
 */

function cqlcache_init() {
    define('CQLCACHE_QUERY_EXPIRY', 'cqlcache_query_expiry');
    define('CQLCACHE_RESULTS_EXPIRY', 'cqlcache_results_expiry');

    define('CQLCACHE_QUERY_EXPIRY_DEFAULT', CACHE_PERMANENT);
    define('CQLCACHE_RESULTS_EXPIRY_DEFAULT', CACHE_PERMANENT);

    define('CQLCACHE_QUERY', 'cache_cql_query');
    define('CQLCACHE_RESULTS', 'cache_cql_result');
    define('CQLCACHE_COUNT', 'cache_cql_count');
}

/**
 * Implementation of hook_menu
 */
function cqlcache_menu() {
    $items = array();

    $items['admin/settings/cqlcache'] = array(
        'title' => 'CQL Cache',
        'description' => 'Control timings on the CQL cache',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('cqlcache_admin_settings'),
        'access arguments' => array('administer site configuration')
    );

    $items['admin/content/cql/cqlcache'] = array(
        'title' => 'CQLCache Clear results',
        'description' => 'Wipe the current cache results',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('cqlcache_clear_form'),
        'access arguments' => array('administer cql'),
    );

    return $items;
}

//====================================================================

/**
 * Cache a parsed query, for the specified period
 */

function cqlcache_set_query($query, $qdata) {
    $cid = cql_cid($query);
    $qdata = serialize($qdata);
    cache_set($cid, CQLCACHE_QUERY, time() + 300, $qdata);
    return $cid;
}

function cqlcache_get_query($query) {
    $qdata = FALSE;
    $cid = cql_cid($query);
    $cached = cache_get($cid, CQLCACHE_QUERY);
    if ($cached->data) {
        $qdata = unserialize($cached->data);
    }
    return $qdata;
}

/**
 * Cache query results & count, for the specified period
 */

function cqlcache_set_results($cid, $results) {
    _cqlcache_set_count($cid, count($results));
    $results = serialize($results);
    cache_set($cid, CQLCACHE_RESULTS, time() + CQLCACHE_RESULTS_EXPIRY_DEFAULT, $results);
}

function cqlcache_get_results($cid) {
    $results = array();
    $cached = cache_get($cid, CQLCACHE_RESULTS);
    if ($cached->data) {
        $results = unserialize($cached->data);
    }
    return $results;
}

/**
 * Cache query count, for the specified period
 */

function _cqlcache_set_count($cid, $count) {
    $count = serialize($count);
    cache_set($cid, CQLCACHE_COUNT, time() + CQLCACHE_RESULTS_EXPIRY_DEFAULT, $count);
}

function cqlcache_get_count($cid) {
    $results = FALSE;
    $cached = cache_get($cid, CQLCACHE_COUNT);
    if ($cached->data) {
        $results = (int) unserialize($cached->data);
    }
    return $results;
}

/**
 * Force a query to occur to find the values of count
 *
 * Rally:US374:TA283
 */

function cqlcache_get_count_force($query) {
    $cid = cql_cid($query);
    $results = cqlcache_get_count($cid);

    if ($results === FALSE) {
        try {
            _cql_load_parser_file();
            _cql_load_mysqlvisitor_file();
            _cql_load_executor_file();

            $parser = new CQLParser($query);
            $parsed = $parser->parsequery();

            $visitor = new cqlVisitorGenMySQL();
            $parsed->accept($visitor);

            $exec = new cqlExecutorMySQL($cid, (string) $visitor);
            $results = cqlcache_get_count($cid);
        }
        catch (Exception $e) {
            watchdog('CQL Cache', '%message', array('%message' => $e->getMessage()));
        }
    }

    return $results;
}

//====================================================================

/**
 * CQL Cache Admin settings
 */

function cqlcache_admin_settings() {

    $form['box1'] = array(
        '#type' => 'fieldset',
        '#title' => t('CQL Query Cache expiry'),
        '#collapsible' => FALSE,
        '#collapsed' => FALSE
    );

    $form['box1'][CQLCACHE_QUERY_EXPIRY] = array(
        '#type' => 'textfield',
        '#title' => t('The time in seconds for the expiry of the CQL Query cache'),
        '#description' => t('When a query is submitted via CQL it must be processed, which is a time consuming activity. CQL caches the processed version of the query, forever by default'),
        '#default_value' => variable_get(CQLCACHE_QUERY_EXPIRY, CQLCACHE_QUERY_EXPIRY_DEFAULT),
        '#size' => 10
    );

    $form['box1'][CQLCACHE_RESULTS_EXPIRY] = array(
        '#type' => 'textfield',
        '#title' => t('The time in seconds for the expiry of the CQL Results cache'),
        '#description' => t('When a query is processed the results are cached to reduce the loading, this is also tied in to the Query used so if two people issue the same query in the same time period only one search will be done'),
        '#default_value' => variable_get(CQLCACHE_RESULTS_EXPIRY, CQLCACHE_RESULTS_EXPIRY_DEFAULT),
        '#size' => 10
    );

    return system_settings_form($form);
}

function cqlcache_admin_settings_submit($form, &$form_state) {
    variable_set(CQLCACHE_QUERY_EXPIRY, $form_state['values'][CQLCACHE_QUERY_EXPIRY]);
}

//====================================================================

/**
 * CQL Cache Admin settings
 */

function cqlcache_clear_form() {

    $form['box1'] = array(
        '#type' => 'fieldset',
        '#collapsible' => FALSE,
        '#collapsed' => FALSE
    );

    $form['box1']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Clear CQL Cache')
    );

    return $form;
}

function cqlcache_clear_form_submit($form, &$form_state) {
    cqlcache_clear_results();
}

/**
 * Clear the results and the counts caches
 */

function cqlcache_clear_results($cid = '*') {
    $wildcard = ($cid=='*');
    cache_clear_all($cid, CQLCACHE_RESULTS, $wildcard);
    cache_clear_all($cid, CQLCACHE_QUERY, $wildcard);
    cache_clear_all($cid, CQLCACHE_COUNT, $wildcard);
}

//====================================================================

/**
 * Intercept node operations and kill the cache if it might have changed
 */

/* FIXME: do we need this function?
function xcqlcache_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
    if (array_key_exists($node->type, drupal_map_assoc(variable_get('cql_forms', array())))) {
        switch ($op) {
            case 'delete':
            case 'insert':
            case 'update':
                $result = db_query('SELECT cid from {cache_cql_result}');
                while ($cid = db_fetch_object($result)) {
                    $nid_array = cqlcache_get_results($cid->cid);
                    if (in_array($node->nid, $nid_array)) {
                        cqlcache_clear_results($cid->cid);
                    }
                }
        }
    }
}
*/
