<?php

/**
 * @file
 * @brief
 * Implement Common Query Language in a Drupal environment.
 *
 * This PHP version was translated from the Python version 1.5
 * by Rob Sanderson (azaroth@liv.ac.uk)  which was distributed
 * and usable under the GPL, for which he was ably assisted by
 * Adam from IndexData and Mike Taylor who provided valuable input
 *
 * @update
 * Handling of default indexes and relations seemed incorrect and
 * generated errors if the context set was omitted, rather than
 * inserting the defaults: changed to insert defaults.
 *
 * @package 	CQL
 * @subpackage
 * @author		
 */

contextset_build(); // Load all the context set files (contextset dependency)

// -- Define Parsing Strictness Flags

define('errorOnEmptyTerm', FALSE);  // index = ""      (often meaningless)
define('errorOnDuplicatePrefix', FALSE);  // >a=b >a=c ""    (impossible due to BNF)
define('errorOnQuotedIdentifier', FALSE); // "/foo/bar" = "" (unnecessary BNF restriction)
define('fullResultSetNameCheck', TRUE); // srw.rsn=foo and srw.rsn=foo (mutant!!)

// -- Base values for CQL

define('serverChoiceIndex', contextSet::get_csi());
define('serverChoiceRelation', contextSet::get_csr());
define('serverContextSet', 'cql');

define('defaultServerIndex', serverContextSet . '.' . serverChoiceIndex);
define('defaultServerRelation', serverContextSet . '.' . serverChoiceRelation);

$GLOBALS['_cql_booleans'] = contextSet::collect_booleans();
$GLOBALS['_cql_endstr'] = array(
                            '"',
                            '/',
                            '=',
                            '<',
                            '>',
                            '(',
                            ')',
                            ' ',
                          );
$GLOBALS['_cql_keywords'] = array(
                          'any',
                          'all',
                          'exact',
                          'and',
                          'or',
                          'not',
                          'xor',
                          'prox',
                          'sortby',
                        );
$GLOBALS['_cql_literal'] = array(
                          '=',
                          '>',
                          '>=',
                          '<',
                          '<=',
                          '<>',
                          '/',
                        );
$GLOBALS['_cql_order'] = array(
                          '=',
                          '>',
                          '>=',
                          '<',
                          '<=',
                          '<>',
                        );
$GLOBALS['_cql_relations'] = contextSet::collect_booleans();
$GLOBALS['_cql_rkeywords'] = array_flip($GLOBALS['_cql_keywords']);

define('escapeChar', '\\');
define('modifierSeparator', "/");

define('CQLAnchor', '^');
define('XCQLIndent', '  ');

// -- End of Configuration

// -- Tokens for Lexical & Syntactical Analysis
// -- The order is somewhat critical as 'any', 'all' and 'exact' are
// -- relations and keywords (actually not technically but I'm treating
// -- as such). So they must be at the end of relations, and start of
// -- keywords.

$cql_numTokens = 0;
define('T_NULL', $cql_numTokens++);
define('T_PAR_OPEN', $cql_numTokens++);
define('T_PAR_CLOSE', $cql_numTokens++);
define('T_GT', $cql_numTokens++);
define('T_LT', $cql_numTokens++);
define('T_GE', $cql_numTokens++);
define('T_LE', $cql_numTokens++);
define('T_EQ', $cql_numTokens++);
define('T_NE', $cql_numTokens++);
define('T_EEQ', $cql_numTokens++);
define('T_SLASH', $cql_numTokens++);
define('T_ANY', $cql_numTokens++);
define('T_ALL', $cql_numTokens++);
define('T_EXACT', $cql_numTokens++);
define('T_AND', $cql_numTokens++);
define('T_OR', $cql_numTokens++);
define('T_NOT', $cql_numTokens++);
define('T_PROX', $cql_numTokens++);
define('T_SORTBY', $cql_numTokens++);
define('T_STRNG', $cql_numTokens++);
define('T_QUOTED', $cql_numTokens++);

/**
 * Allow any class that uses this to accept a visit from
 * another object, the Visitor will be for re-generating
 * the structure as different forms of output (e.g. XML
 * and MySQL 'where' clause)
 *
 * And removes that type of code from this class, keeping
 * it slim and lithe
 */

abstract class Visitation {
  /**
   * Description needed.
   */
  function accept(cqlVisitor $visitor, $depth = 0) {
    $visitor->visit($this, $depth);
  }
}

abstract class Parentable extends Visitation {
  protected $parent = NULL;

  /**
   * Description needed.
   */
  protected function set_parent(&$parent) {
    $this->parent =& $parent;
  }

  /**
   * Description needed.
   */
  public function __toString() {
    return ':Parentable' ;
  }
}

/**
 * Treat modifiers as keys on boolean/relation?
 */
abstract class ModifiableObject extends Parentable {
  private $modifiers = array();

  /**
   * Description needed.
   */
  protected function getitem($k) {
    if (is_int($k)) {
      return $this->modifiers[$k];
    }
    else {
      if (!empty($this->modifiers)) {
        foreach ($this->modifiers as $modifier) {
          if ($modifier->type==$k) {
            return $modifier;
          }
          elseif ($modifier->type->value==$k) {
            return $modifier;
          }
        }
      }
      return NULL;
    }
  }

  /**
   * Description needed.
   */
  public function __toString() {
    return ':ModifiableObject' . parent::__toString();
  }
}

/**
 * Root object for triple and searchClause
 */

abstract class PrefixableObject extends Parentable {

  private $prefixes = array();
  private $config = NULL;

  protected static $reservedPrefixes = FALSE;

  /**
   * Description needed.
   */
  protected function __construct() {
    if (!PrefixableObject::$reservedPrefixes) {
      PrefixableObject::$reservedPrefixes = contextSet::get_prefix_set();
    }
  }

  /**
   * Add a prefix to this object
   * If we're checking for duplicates throw an exception if we find one
   *
   * @param $name
   * Name of the prefix to store
   *
   * @param $identifier
   * The string that the name represents
   *
   */
  public function addPrefix($name, $identifier) {
    if (errorOnDuplicatePrefix) {
      if (array_key_exists($name, $this->prefixes)) {
        throw new Exception('Defined duplicate prefix "' . $name . '"');
      }
      elseif (array_key_exists($name, PrefixableObject::$reservedPrefixes)) {
        throw new Exception('Defined reserved prefix "' . $name . '"');
      }
    }
    $this->prefixes[$name] = $identifier;
  }

  /**
   * Resolve a prefix from this or parent
   * Given a prefix see whether it's a reserved one or one of mine
   * if it's not try a parent, and if I don't have a parent try the
   * special config object (if we have one)
   *
   * @param $name
   * Name of the prefix to be found
   *
   * @return
   * The identifier represented by the prefix, or NULL
   *
   */
  public function resolvePrefix($name) {
    if (array_key_exists($name, $this->reservedPrefixes)) {
      return $this->reservedPrefixes[$name];
    }
    elseif (array_key_exists($name, $this->prefixes)) {
      return $this->prefixes[$name];
    }
    elseif ($this->parent) {
      return $this->parent->resolvePrefix($name);
    }
    elseif ($this->config) {
      return $this->config($name);
    }
    return NULL;
  }

  /**
   * Description needed.
   */
  public function __toString() {
    return ':Prefixable' . parent::__toString();
  }
}

/**
 * Root object for relation, relationModifier and index
 *
 * As PHP5 doesn't support multiple inheritance and 'Relation'
 * needs to inherit from ModifiableObject and PrefixableObject
 * we are naughty here and extend PrefixedObject from Modifiable,
 * it's not good but it won't do any actual harm
 */

abstract class PrefixedObject extends ModifiableObject {
  protected $prefix = '' ;
  protected $prefixURI = '' ;
  protected $value = '' ;

  /**
   * Description needed.
   */
  public function PrefixedObject($val) {
    $val = drupal_strtolower($val);
    if ($val) {
      if (drupal_substr($val, 0) == '"' && drupal_substr($val, -1) == '"') {
        if (errorOnQuotedIdentifier) {
          throw new Exception('Quoted identifier not permitted ' . $val);
        }
        else {
          $val = drupal_substr($val, 1, -1);
        }
      }
    }
    $this->value = $val;
    $this->splitvalue();
  }

  /**
   * Description needed.
   */
  public function __get($var) {
    if (isset($this->$var)) {
      return $this->$var;
    }
  }

  /**
   * Description needed.
   */
  public function __toString() {
    if ($this->prefix) {
      return $this->prefix . '.' . $this->value;
    }
    else {
      return $this->value;
    }
  }

  /**
   * Description needed.
   */
  private function splitvalue() {
    $bits = explode('.', $this->value);

    switch (count($bits)) {
      case 1:
        $this->prefix = ($contextSet = contextSet::find_index($this->value)) ? $contextSet :
        ($contextSet = contextSet::find_relation($this->value)) ? $contextSet :
        ($contextSet = contextSet::find_relmod($this->value)) ? $contextSet :
        serverContextSet;
        $this->value = $bits[0];
        break;
      case 2:
        $this->prefix = $bits[0];
        $this->value = $bits[1];
        break;
      default:
        throw new Exception('Multiple "." characters in "' . $this->value . '"');
        break;
    }
  }

  /**
   * Description needed.
   */
  public function resolvePrefix() {
    if ($this->prefixURI=='') {
      $this->prefixURI = $this->parent->resolvePrefix($this->prefix);
    }
    return $this->prefixURI;
  }


}

/**
 * Object to represent a CQL triple
 */

class Triple extends PrefixableObject {
  private $leftOperand = NULL;
  private $xBoolean = NULL;
  private $rightOperand = NULL;

  /**
   * Description needed.
   */
  public function Triple() {
    parent::__construct();
  }

  /**
   * Description needed.
   */
  public function set_left(&$obj) {
    $this->leftOperand = $obj;
    $obj->set_parent($this);
  }

  /**
   * Description needed.
   */
  public function set_bool(&$obj) {
    $this->xBoolean = $obj;
    $obj->set_parent($this);
  }

  /**
   * Description needed.
   */
  public function set_right(&$obj) {
    $this->rightOperand = $obj;
    $obj->set_parent($this);
  }

  /**
   * Description needed.
   */
  public function __get($var) {
    if (isset($this->$var)) {
      return $this->$var;
    }
    return NULL;
  }

  /**
   * Description needed.
   */
  public function getResultSetId($top = NULL) {
    if (!fullResultSetNameCheck || in_array($this->xBoolean->value, array('not', 'prox'))) {
      return NULL;
    }

    if ($top === NULL) {
      $toplevel = TRUE;
      $top = $this;
    }
    else {
      $toplevel = FALSE;
    }

    // -- Iterate over operands and build a list of them all

    $rslist = array();

    if (is_a($this->leftOperand, 'Triple')) {
      $rslist = array_merge($rslist, $this->leftOperand->getResultSetId($top));
    }
    else {
      $rslist[] = $this->leftOperand->getResultSetId($top);
    }

    if (is_a($this->rightOperand, 'Triple')) {
      $rslist = array_merge($rslist, $this->rightOperand->getResultSetId($top));
    }
    else {
      $rslist[] = $this->rightOperand->getResultSetId($top);
    }

    if ($toplevel == 1) {
      // -- Check all elements are the same, if so we're a fubar form of present
      $count = 0;
      $test = NULL;
      foreach ($rslist as $rs) {
        if ($test === NULL) {
          $test = $rs;
          $count++ ;
        }
        else {
          if ($test==$rs) {
            $count++ ;
          }
        }
      }
      if (count($rslist) == $count) {
        return $rslist[0];
      }
      else {
        return array();
      }
    }
    return $rslist;
  }

  /**
   * Description needed.
   */
  public function __toString() {
    return ':Triple' . parent::__toString();
  }
}

/**
 * Object to represent a CQL searchClause
 */

class SearchClause extends PrefixableObject {
  private $index = NULL;
  private $relation = NULL;
  private $term = NULL;

  /**
   * Description needed.
   */
  public function SearchClause($ind, $rel, $term) {

    $this->index = $ind;
    $this->relation = $rel;
    $this->term = $term;

    $ind->parent = $this;
    $rel->parent = $this;
    $term->parent = $this;
  }

  /**
   * Description needed.
   */
  public function __get($var) {
    if (isset($this->$var)) {
      return $this->$var;
    }
    return NULL;
  }

  /**
   * Description needed.
   */
  public function getResultSetId() {
    $this->index->resolvePrefix();
    if ($this->index->get_prefixuri()==$this->reservedPrefixes['cql']) {
      if (drupal_strtolower($this->index->get_prefixuri()) == 'resultsetid') {
        $this->term->get_value();
      }
    }
    return '' ;
  }

  /**
   * Description needed.
   */
  public function __toString() {
    return ':SearchClause' . parent::__toString();
  }
}

/**
 * Object to represent a CQL index
 */

class Index extends PrefixedObject {
  /**
   * Description needed.
   */
  public function __toString() {
    return $this->prefix . '.' . $this->value;
  }
}

/**
 * Object to represent a CQL relation
 */

class Relation extends PrefixedObject {

  /**
   * Description needed.
   */
  public function Relation($rel, $mods = array()) {
    parent::PrefixedObject($rel);
    $this->modifiers = $mods;
  }

  /**
   * Description needed.
   */
  public function __toString() {
    return $this->prefix . '.' . $this->value;
  }

  /**
   * Description needed.
   */
  public function set_prefix($r) {
    if ($this->prefix == 'cql') {
      $this->prefix = $r;
    }
  }

  /**
   * Description needed.
   */
  public function get_modifiers() {
    $mods = array();
    if (!empty($this->modifiers)) {
      foreach ($this->modifiers as &$m) {
        $mods[] = $m->fetch();
      }
    }
    return $mods;
  }
}

/**
 * Object to represent a CQL term
 */
class Term extends Visitation {
  private $value;

  /**
   * Description needed.
   */
  public function Term($v) {
    global $_cql_literal;

    if (drupal_substr($v, 0) == '"' && drupal_substr($v, -1) == '"') {
      $v = drupal_substr($v, 1, -1);
      $v = stripslashes($v);
    }

    if ($v=='') {
      if (errorOnEmptyTerm) {
        throw new Exception('Empty term found');
      }
    }
    if (in_array($v, $_cql_literal)) {
      throw new Exception('Unquoted literal found "' . $v . '"');
    }
    if ($v == str_repeat(CQLAnchor, drupal_strlen($v))) {
      throw new Exception('Only anchoring characters found in term "' . $v . '"');
    }

    $this->value = $v;
    return $v;
  }

  /**
   * Description needed.
   */
  public function __toString() {
    return $this->value;
  }
}

/**
 * Object to represent a CQL boolean
 */
class xBoolean extends ModifiableObject {
  private $value = NULL;

  /**
   * Description needed.
   */
  public function xBoolean($xbool, $mods = array()) {
    $this->value = $xbool;
    $this->modifiers = $mods;
  }

  /**
   * Description needed.
   */
  public function __get($var) {
    if (isset($this->$var)) {
      return $this->$var;
    }
  }

  /**
   * Description needed.
   */
  public function resolvePrefix($name) {
    return $this->parentresolvePrefix($name);
  }

  /**
   * Description needed.
   */
  public function __toString() {
    return $this->value;
  }

}

/**
 * Object to represent a CQL Modifier type
 */
class ModifierType extends PrefixedObject {
  // Same as index, but we'll XCQLify in ModifierClause
  // and the output is in the parent object
}

/**
 * Object to represent a CQL relation modifier
 */

class ModifierClause extends Visitation {
  private $type = NULL;
  private $comparison = '' ;
  private $value = '' ;

  /**
   * Description needed.
   */
  public function ModifierClause($type, $comp = '', $val = '') {
    $this->type = new ModifierType($type);
    $this->comparison = $comp;
    $this->value = $val;
  }

  /**
   * Description needed.
   */
  public function __toString() {
    if ($this->value) {
      return $this->type . $this->comparison . $this->value;
    }
    else {
      return $this->value;
    }
  }

  /**
   * Description needed.
   */
  public function fetch() {
    return array('type' => (string) $this->type, 'comparison' => $this->comparison, 'value' => $this->value);
  }
}

/**
 * Parser for CQL query, uses the cache if available
 */

class CQLParser {
  private $query = '' ;
  private $result;
  private $cid;

  private $tokens = array();
  private $curToken = NULL;
  private $nxtToken = NULL;

  /**
   * Description needed.
   */
  public function CQLParser($q) {
    $this->query = trim($q);
  }

  /**
   * Description needed.
   */
  public function parsequery() {
    if (!$this->query) {
      return FALSE;
    }
    if (!$this->result) {
      $lexer = new LexicalAnalyser($this->query);
      $lexer->go($this->tokens);

      $this->fetch_token(TRUE);
      $this->fetch_token();
      $this->result = $this->query();
    }
    if (module_exists('cqlcache')) {
      $this->cid = cqlExecutor::make_cid($this->result);
    }
    return $this->result;
  }

  /**
   * Description needed.
   */
  public function cid() {
    return $this->cid;
  }

  /**
   * Description needed.
   */
  public function result() {
    return $this->result;
  }

  /**
   * Description needed.
   */
  private function is_boolean($token) {
    global $_cql_booleans;
    return (in_array($token->v, $_cql_booleans));
  }

  /**
   * Description needed.
   */
  private function is_relation($token) {
    global $_cql_relations;
    return (in_array($token->v, $_cql_relations));
  }

  /**
   * Description needed.
   */
  private function fetch_token($reset = FALSE) {
    static $tp = 0;
    if ($reset) {
      $tp = 0;
    }
    $this->curToken = $this->nxtToken;
    $this->nxtToken = $this->tokens[$tp++]; // FIXME: Undefined offset
  }

  // -- Create prefixes dictionary

  /**
   * Description needed.
   */
  private function prefixes() {
    $prefs = array();
    $count = 0;
    while ($this->curToken->t == T_GT) {
      if ($count++>20) {
        throw new Exception('Too many prefixes defined at one time');
      }
      $this->fetch_token();
      if ($this->nxtToken->t == T_EQ) {
        $name = $this->curToken->v;
        $this->fetch_token(); // '=' is current
        $this->fetch_token(); // 'id' is current
        $ident = $this->curToken->v;
        $this->fetch_token();
      }
      else {
        $name = '' ;
        $ident = $this->curToken->v;
        $this->fetch_token();
      }
      $name = drupal_strtolower($name);
      if (errorOnDuplicatePrefix) {
        if (array_key_exists($name, $prefs)) {
          throw new Exception('Duplicate prefix "' . $name . '"');
        }
      }
      if (drupal_strlen($ident)) {
        if (drupal_substr($ident, 0) == '"' && drupal_substr($ident, 0) == '"') {
          $ident = drupal_substr($ident, 1, -1);
        }
      }
      $prefs[$name] = $ident;
    }
    return $prefs;
  }

  // -- Parse a query

  /**
   * Description needed.
   */
  private function query() {
    static $endings = NULL;

    if ($endings === NULL) {
      $endings = array(T_PAR_CLOSE, T_NULL);
    }

    $prefs = $this->prefixes();
    $left = $this->subQuery();
    $count = 0;
    while (!in_array($this->curToken->t, $endings)) {
      if ($count++>20) {
        throw new Exception('Query too complex at this level');
      }
      if ($this->is_boolean($this->curToken)) {
        $boolobject = $this->xBoolean();
        $right = $this->subQuery();

        $trip = new Triple();
        $trip->set_left($left);
        $trip->set_bool($boolobject);
        $trip->set_right($right);

        $left = $trip;
      }
    }
    $this->add_prefixes($left);
    return $left;
  }

  // -- Add prefixes (if any) to an object

  /**
   * Description needed.
   */
  private function add_prefixes(&$obj) {
    if (!empty($prefs)) {
      foreach ($prefs as $name => $ident) {
        $obj->addPrefix($name, $ident);
      }
    }
  }

  // -- Find either query or clause

  /**
   * Description needed.
   */
  private function subQuery() {
    if ($this->curToken->t == T_PAR_OPEN) {
      $this->fetch_token();
      $obj = $this->query();
      if ($this->curToken->t == T_PAR_CLOSE) {
        $this->fetch_token();
      }
      else {
        throw new Exception('Missing closing ")"');
      }
    }
    else {
      $prefs = $this->prefixes();
      if (!empty($prefs)) {
        $obj = $this->query();
        $this->add_prefixes($obj);
      }
      else {
        $obj = $this->clause();
      }
    }
    return $obj;
  }

  // -- Find searchClause
  // -- this is the tricky one because it has to recognise whether we've
  // -- got a Term only, Index+Relation+Term, or a Prefixed Clause

  /**
   * Description needed.
   */
  private function clause() {

    $next_is_bool = $this->is_boolean($this->nxtToken);
    $next_is_relation = $this->is_relation($this->nxtToken);
    $next_is_thing1 = in_array($this->nxtToken->t, array(T_PAR_OPEN, T_PAR_CLOSE, T_NULL));
    $next_is_thing2 = in_array($this->nxtToken->t, array(T_PAR_CLOSE, T_NULL));

    // -- now check for full Index+Relation+Term

    if (!$next_is_bool && !$next_is_thing1) {
      $index = new Index($this->curToken->v);
      $this->fetch_token();
      $rel = $this->relation();
      if ($this->curToken->t == T_NULL) {
        exit('Expected a Term but got end of query');
        throw new Exception('Expected a Term but got end of query');
      }
      if ($this->curToken->t == T_PAR_CLOSE) {
        exit('Expected a Term but got ")"');
        throw new Exception('Expected a Term but got ")"');
      }
      $term = new Term($this->curToken->v);
      $this->fetch_token();
      contextSet::check_irt_syntax($index, $rel, $term);
      $irt = new searchClause($index, $rel, $term);
      return $irt;
    }

    // -- now check for Term on its own

    elseif ($this->curToken->t != T_PAR_CLOSE && $this->curToken->t != T_NULL && ($next_is_bool || $next_is_thing2)) {
      $index = new Index(serverChoiceIndex);
      $rel = new Relation(serverChoiceRelation);
      $term = new Term($this->curToken->v);
      $this->fetch_token();
      contextSet::check_irt_syntax($index, $rel, $term);
      $irt = new searchClause($index, $rel, $term);
      return $irt;
    }

    // -- is this a prefixed clause? get all the prefixes and then get the clause

    elseif ($this->curToken->t == T_GT) {
      $prefs = $this->prefixes();
      $obj = $this->clause();
      $this->add_prefixes($obj);
      return $obj;
    }

    // -- otherwise it's an error

    throw new Exception('Unexpected "' . $this->curToken->v . '" in query');
  }

  /**
   * Description needed.
   */
  private function modifiers() {
    global $_cql_order;
    $mods = array();

    $count = 0;
    while ($this->curToken->t == T_SLASH) {
      if ($count++>20) {
        throw new Exception('Too many modifiers for one relations');
      }
      $this->fetch_token();
      if ($this->curToken->t == T_SLASH) {
        throw new Exception('Missing modifier');
      }
      $mod = drupal_strtolower($this->curToken->v);
      $this->fetch_token();
      $comp = $this->curToken->v;
      if (in_array($comp, $_cql_order)) {
        $this->fetch_token();
        $value = $this->curToken->v;
        $this->fetch_token();
      }
      else {
        $comp = '' ;
        $value = '' ;
      }
      $mods[] = new ModifierClause($mod, $comp, $value);
    }
    return $mods;
  }

  /**
   * Description needed.
   */
  private function xBoolean() {
    if ($this->is_boolean($this->curToken)) {
      $xb = $this->curToken->v;
      $this->fetch_token();
      $xBool = new xBoolean($xb, $this->modifiers());
    }
    else {
      throw new Exception('Expecting a boolean, but got: ' . $this->curToken->v);
    }
    return $xBool;
  }

  /**
   * Description needed.
   */
  private function relation() {
    $rv = drupal_strtolower($this->curToken->v);
    $this->fetch_token();
    return new Relation($rv, $this->modifiers());
  }
}

/**
 * Token for CQL query
 */
class Token {
  public $t = 0;
  public $v = '';

  /**
   * Description needed.
   */
  public function Token($t, $v) {
    $this->t = $t;
    $this->v = $v;
  }
}

/**
 * Lexical Analyser for CQL query
 *
 * This converts a query into its tokens returning them
 * as an array of token objects containing the token type
 * and the string of the token itself.
 *
 */
class LexicalAnalyser {
  private $qry = '' ;
  private $idx = 0;
  private $len = 0;
  private $tokens = array();

  /**
   * Description needed.
   */
  public function LexicalAnalyser($q) {
    if ($q=='') {
      throw new Exception('No query supplied');
    }
    $this->qry = preg_replace('/\s\s+/', ' ', $q);
    $this->len = drupal_strlen($this->qry);
  }

  /**
   * Description needed.
   */
  public function go(&$tokens) {
    global $_cql_keywords, $_cql_rkeywords, $_cql_endstr;
    $c = $this->getchar();
    $count = 0;
    while (TRUE) {
      if ($count++ > 500) {
        throw new Exception('Surprisingly long query: ' . $this->qry);
      }
      switch ($c) {
        case ' ':
          $c = $this->getchar();
          break;
        case '(':
          $tokens[] = new Token(T_PAR_OPEN, '(');
          $c = $this->getchar();
          break;
        case ')':
          $tokens[] = new Token(T_PAR_CLOSE, ')');
          $c = $this->getchar();
          break;
        case modifierSeparator:
          $tokens[] = new Token(T_SLASH, '/');
          $c = $this->getchar();
          break;
        case '=':
          $c = $this->getchar();
          if ($c=='=') {
            $tokens[] = new Token(T_EEQ, '==');
            $c = $this->getchar();
          }
          else {
            $tokens[] = new Token(T_EQ, '=');
          }
          break;
        case '>':
          $c = $this->getchar();
          if ($c=='=') {
            $tokens[] = new Token(T_GE, '>=');
            $c = $this->getchar();
          }
          else {
            $tokens[] = new Token(T_GT, '>');
          }
          break;
        case '<':
          $c = $this->getchar();
          if ($c=='=') {
            $tokens[] = new Token(T_LE, '<=');
            $c = $this->getchar();
          }
          elseif ($c=='>') {
            $tokens[] = new Token(T_NE, '<>');
            $c = $this->getchar();
          }
          else {
            $tokens[] = new Token(T_LT, '<');
          }
          break;
        case '"':
          $s = '' ;
          while (TRUE) {
            $s .= $c;
            $c = $this->getchar();
            if ($c === NULL) {
              throw new Exception('Incomplete quoted string: "' . $s . '"');
            }
            if ($c=='"') {
              $s .= $c;
              $c = $this->getchar();
              break;
            }
            if ($c == escapeChar) {
              $c = $this->getchar();
              if ($c=='"') {
                $s .= $c;
              }
              else {
                $s .= escapeChar . $c;
              }
              $c = $this->getchar();
            }
          }
          $tokens[] = new Token(T_QUOTED, $s);
          break;
        default:
          $s = '' ;
          while (TRUE) {
            $s .= $c;
            $c = $this->getchar();
            if (in_array($c, $_cql_endstr) || $c === NULL) {
              break;
            }
          }
          $s = trim($s);

          if ($s > '') {
            $x = drupal_strtolower($s);
            if (in_array($x, $_cql_keywords)) {
              $tokens[] = new Token($_cql_rkeywords[$x] + T_ANY, $s);
            }
            else {
              $tokens[] = new Token(T_STRNG, $s);
            }
          }
          break;
      }
      if ($c == NULL) {
        break;
      }
    }
  }

  /**
   * Description needed.
   */
  private function getchar() {
    $q =& $this->qry;
    $i =& $this->idx;

    if ($i >= $this->len) {
      return NULL;
    }
    return drupal_substr($q, $i++, 1);
  }
}

